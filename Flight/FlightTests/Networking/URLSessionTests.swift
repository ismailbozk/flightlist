//
//  URLSessionTests.swift
//  FlightTests
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import XCTest
import Flight

class URLSessionTests: XCTestCase {

    var urlSession: URLSession?
    
    override func setUp() {
        urlSession = URLSession(configuration: URLSessionConfiguration.default)
    }

    func test404() {
        let expectation = XCTestExpectation(description: "Http request test with 404 response")
        let url = URL(string: "https://www.reddit.com/23reefsdfds")!
        let request = URLRequest(url: url)
        urlSession?.perform(request: request, completion: { (result) in
            switch result {
            case .success(let response, _):
                XCTAssertTrue(response is HTTPURLResponse)
                XCTAssertTrue((response as? HTTPURLResponse)?.statusCode == 404)
            case .failure:
                XCTAssertTrue(false)
            }
            
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 60)
    }
    
    func test200() {
        let expectation = XCTestExpectation(description: "Http request test with 200 response")
        let url = URL(string: "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=59f1221c42b07eddaf48f3c7b3c1447d&tags=Dsfs&page=1&format=json&nojsoncallback=1")!
        let request = URLRequest(url: url)
        urlSession?.perform(request: request, completion: { (result) in
            switch result {
            case .success(let response, _):
                XCTAssertTrue(response is HTTPURLResponse)
                XCTAssertTrue((response as? HTTPURLResponse)?.statusCode == 200)
            case .failure:
                XCTAssertTrue(false)
            }
            
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 60)
    }
    
    func testCancelled() {
        let expect = expectation(description: "Http request cancel test")
        let url = URL(string: "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=59f1221c42b07eddaf48f3c7b3c1447d&tags=Dsfs&page=1&format=json&nojsoncallback=1")!
        let request = URLRequest(url: url)
        let dataTask = urlSession?.perform(request: request, completion: { (result) in
            switch result {
            case .success:
                XCTAssertTrue(false)
            case .failure(let error):
                XCTAssertTrue(error.code.rawValue == NSURLErrorCancelled)
            }
            
            expect.fulfill()
        })
        dataTask?.cancel()
        
        wait(for: [expect], timeout: 5)
    }
}
