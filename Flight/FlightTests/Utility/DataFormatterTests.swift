//
//  DataFormatterTests.swift
//  FlightTests
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import XCTest
@testable import Flight

class DataFormatterTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

  
    func testFullDate() {
        let dateString = "2019-12-20T10:10"

        guard let date = DateFormatter.dateFormatter.date(from: dateString) else {
            XCTAssertTrue(false)
            return
        }
    }

    
    func testDisplayedTime() {
        let dateString = "2020-10-31T22:35"
        let parserFormatter = DateFormatter.dateFormatter
        
        guard let date = parserFormatter.date(from: dateString) else {
            
            XCTAssertTrue(false)
            return
        }
        
        let displayFormatter = DateFormatter.displayDateFormatter
        displayFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        let formattedTime = displayFormatter.string(from: date)

        XCTAssertTrue(formattedTime == "10:35 PM")
    }
    
    func testDisplayedTime2() {
        let dateString = "2020-10-31T22:35"
        let parserFormatter = DateFormatter.dateFormatter
        
        guard let date = parserFormatter.date(from: dateString) else {
            
            XCTAssertTrue(false)
            return
        }
        
        let displayFormatter = DateFormatter.displayDateFormatter
        displayFormatter.locale = Locale(identifier: "en_GB")
        
        let formattedTime = displayFormatter.string(from: date)
        
        XCTAssertTrue(formattedTime == "22:35")
    }
    
    
    func testDisplayedTimeWithDifferentDeviceTimeZones() {
        let dateString = "2020-10-31T22:35"
        let timeZone = TimeZone(identifier: "GMT-6")
        let parserFormatter = DateFormatter.dateFormatter
        parserFormatter.timeZone = timeZone
        guard let date = parserFormatter.date(from: dateString) else {
            
            XCTAssertTrue(false)
            return
        }
        
        let displayFormatter = DateFormatter.displayDateFormatter
        displayFormatter.locale = Locale(identifier: "en_GB")
        displayFormatter.timeZone = timeZone
        
        let formattedTime = displayFormatter.string(from: date)
        
        XCTAssertTrue(formattedTime == "22:35")
    }
    
    func testLegDates() {
        let leg = Leg(id: "1",
                      departureAirport: "JFK",
                      arrivalAirport: "GLA",
                      departureTime: "2020-10-30T22:35",
                      arrivalTime: "2020-10-31T12:35",
                      stops: 0,
                      airlineName: "THY",
                      airlineId: "THY",
                      durationMins: 400)
        
        XCTAssertNotNil(leg.departureDate)
        XCTAssertNotNil(leg.arrivalDate)
    }
}
