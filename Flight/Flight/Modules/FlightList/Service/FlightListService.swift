//
//  FlightListService.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class FlightListService {
    //https://s3-eu-west-1.amazonaws.com/skyscanner-prod-takehome-test/flights.json
    
    private let urlSession: NetworkUrlSession
    private let responseParser: HTTPResponseParser
    private let decoder: JSONDecoder
    
    init(urlSession: NetworkUrlSession,
         responseParser: HTTPResponseParser = HTTPResponseParser(),
         decoder: JSONDecoder = JSONDecoder()) {
        self.urlSession = urlSession
        self.responseParser = responseParser
        self.decoder = decoder
    }
    
    func fetchFlightList(completion: @escaping (Result<FlightList, Error>) -> Void) {
        guard let url = URL(string: "https://s3-eu-west-1.amazonaws.com/skyscanner-prod-takehome-test/flights.json") else {
            assertionFailure("the flight url is not correct")
            return
        }
        let request = URLRequest(url: url)
        urlSession.perform(request: request) { (result) in
            
            
            let parsedResult: Result<FlightList, Error>

            switch result {
            case .success(let response, let data):
                parsedResult = self.handleFetchPhotosSuccessResponse(response: response, data: data)

            case .failure(let error):
                parsedResult = .failure(error)
            }
            completion(parsedResult)
        }
    }
    
    private func handleFetchPhotosSuccessResponse<T: Decodable>(response: URLResponse, data: Data) -> Result<T, Error> {
        let result: Result<T, HTTPResponseParserError> = responseParser.parse(response: response, data: data)
        
        switch result {

        case .success(let value):
            return .success(value)
        case .failure(let error):
            return .failure(error)
        }        
    }
}
