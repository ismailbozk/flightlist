//
//  FlightListPresenter.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class FlightListPresenter {
    private let interactor: FlightListInteractor
    private weak var view: FlightListView?
    
    init(interactor: FlightListInteractor, view: FlightListView) {
        self.interactor = interactor
        self.view = view
    }
}

extension FlightListPresenter: FlightListEventHandler {
    func viewDidLoad() {
        self.interactor.fetchFlightList { (result) in
            switch result {
            case .success(let items):
                self.view?.displayFlights(items: items)
            case .failure(_): break
                // handle errors
            }
        }
    }
}
