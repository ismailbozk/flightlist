//
//  FlightListEventHandler.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

protocol FlightListEventHandler: AnyObject {
    func viewDidLoad()
}
