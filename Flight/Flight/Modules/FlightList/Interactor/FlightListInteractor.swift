//
//  FlightListInteractor.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class FlightListInteractor {
    private let service: FlightListService
    
    init(service: FlightListService) {
        self.service = service
    }
    
    func fetchFlightList(completion: @escaping (Result<[FlightItem], Error>) -> Void) {
        service.fetchFlightList { (result) in
            let processedResult: Result<[FlightItem], Error>
            switch result {
            case .success(let flightlist):                
                let flightItems = flightlist.itineraries.map { itinerary in
                    FlightItem(itinerary: itinerary, legs: self.legs(for: itinerary.legs, in: flightlist.legs))
                }
                 processedResult = .success(flightItems)
            case .failure(let error):
                processedResult = .failure(error)
            }
            
            DispatchQueue.main.async {
                completion(processedResult)
            }
        }
    }
    
    private func legs(for legIds: [String], in legs: [Leg]) -> [Leg] {
        
        return legs.filter { legIds.contains($0.id) }
    }
}

struct FlightItem {
    let itinerary: Itinerary
    let legs: [Leg]
}
