//
//  Entity.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

struct FlightList: Decodable {
    let itineraries: [Itinerary]
    let legs: [Leg]
}

struct Leg: Decodable {
    /**
        {
          "id": "leg_9",
          "departure_airport": "SFO",
          "arrival_airport": "FRA",
          "departure_time": "2020-11-03T10:30",
          "arrival_time": "2020-11-04T06:55",
          "stops": 2,
          "airline_name": "easyJet",
          "airline_id": "EZ",
          "duration_mins": 630
        },
     */
    
    let id: String
    let departureAirport: String
    let arrivalAirport: String
    let departureTime: String
    let arrivalTime: String
    let stops: Int
    let airlineName: String
    let airlineId: String
    let durationMins: Int
    
    var arrivalDate: Date? {
        return DateFormatter.dateFormatter.date(from: arrivalTime)
    }
    var departureDate: Date? {
        return DateFormatter.dateFormatter.date(from: departureTime)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case departureAirport = "departure_airport"
        case arrivalAirport = "arrival_airport"
        case departureTime = "departure_time"
        case arrivalTime = "arrival_time"
        case stops
        case airlineName = "airline_name"
        case airlineId = "airline_id"
        case durationMins = "duration_mins"
    }
}


struct Itinerary: Decodable {
    /**
     {
       "id": "it_5",
       "legs": [
         "leg_1",
         "leg_6"
       ],
       "price": "£195",
       "agent": "Trip.com",
       "agent_rating": 9.5
     },
     */
    
    let id: String
    let legs: [String]
    let price: String
    let agent: String
    let agentRating: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case legs
        case price
        case agent
        case agentRating = "agent_rating"
    }
}
