//
//  FlightListBuilder.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

class FlightListBuilder {
    
    func build() -> UIViewController {
        let urlSession = URLSession.shared
        let service = FlightListService(urlSession: urlSession)
        let interactor = FlightListInteractor(service: service)
        
        let view = viewController
        
        let presenter = FlightListPresenter(interactor: interactor, view: view)
        view.eventHandler = presenter
        
        return view
    }
    
    
    private var viewController: FlightListViewController {
        return storyboard.instantiateViewController(withIdentifier: String(describing: FlightListViewController.self)) as! FlightListViewController
    }

    private var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
