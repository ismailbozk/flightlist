//
//  ViewController.swift
//  Flight
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit
import Kingfisher

class FlightListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "LegCell", bundle: nil), forCellReuseIdentifier: "LegCell")
            tableView.register(UINib(nibName: "FlightCell", bundle: nil), forCellReuseIdentifier: "FlightCell")
        }
    }
    
    var eventHandler: FlightListEventHandler?
    
    var flightItems: [FlightItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventHandler?.viewDidLoad()
    }
}

extension FlightListViewController: FlightListView {
    func displayFlights(items: [FlightItem]) {
        self.flightItems = items
        tableView.reloadData()
    }
}


extension FlightListViewController: UITableViewDelegate {
    
}

extension FlightListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return flightItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flightItems[section].legs.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // flightcell
        let legs = flightItems[indexPath.section].legs
        let itinerary = flightItems[indexPath.section].itinerary
        if legs.count == indexPath.row {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FlightCell", for: indexPath) as! FlightCell
            cell.itinerary = itinerary
            return cell
        }
        else if indexPath.row < legs.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LegCell", for: indexPath) as! LegCell
            cell.leg = legs[indexPath.row]
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    
}
