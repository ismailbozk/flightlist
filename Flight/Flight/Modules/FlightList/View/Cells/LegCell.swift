//
//  LegCell.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

extension UITableViewCell {
    var identifier: String {
        return String(describing: self)
    }
}

class LegCell: UITableViewCell {

    var leg: Leg? {
        didSet {
            guard let leg = leg else {
                return
            }
            durationLabel.text = "\(leg.durationMins%60)h \(leg.durationMins/60)m"
            typeLabel.text = leg.stops > 1 ? "\(leg.stops) Stops" : "Direct"
            if let url = URL(string: "https://logos.skyscnr.com/images/airlines/small/\(leg.airlineId).png") {
                flightIcon.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"))
            }
            else {
                flightIcon.image = UIImage(named: "placeholder")
            }
            let df = DateFormatter.displayDateFormatter
            if let departureDate = leg.departureDate, let arrivalDate = leg.arrivalDate {
                timeLabel.text = "\(df.string(from: departureDate)) - \(df.string(from: arrivalDate))"
            }
            else {
                timeLabel.text = ""
            }
            airportLabel.text = "\(leg.departureAirport)-\(leg.arrivalAirport), \(leg.airlineName)"
        }
    }
    
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var airportLabel: UILabel!
    
    @IBOutlet weak var flightIcon: UIImageView!
    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        flightIcon.image = nil
        timeLabel.text = ""
        airportLabel.text = ""
        durationLabel.text = ""
        typeLabel.text = ""
    }
    
}
