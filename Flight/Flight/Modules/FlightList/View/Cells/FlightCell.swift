//
//  FlightCell.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import UIKit

class FlightCell: UITableViewCell {

    var itinerary: Itinerary? {
        didSet {
            priceLabel.text = itinerary?.price ?? ""
            viaLabel.text = "via \(itinerary?.agent ?? "")"
        }
    }
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var viaLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
