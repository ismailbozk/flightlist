//
//  FlightListView.swift
//  Flight
//
//  Created by Bozkurt on 11/22/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

protocol FlightListView: AnyObject {
    func displayFlights(items: [FlightItem])
}
