//
//  Service.swift
//  Flight
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

final class Service {
    private let urlSession: NetworkUrlSession
    private let responseParser: HTTPResponseParser
    private let decoder: JSONDecoder
    
    init(urlSession: NetworkUrlSession,
         responseParser: HTTPResponseParser = HTTPResponseParser(),
         decoder: JSONDecoder = JSONDecoder()) {
        self.urlSession = urlSession
        self.responseParser = responseParser
        self.decoder = decoder
    }
    
    /// Performs a http request
    @discardableResult
    func fetchPhotos<T: Decodable>(completion: @escaping (Result<T, Error>) -> Void) -> NetworkUrlSessionDataTask {
        let request = photosRequest(with: "Tea Pot", page: 0)
        let task = urlSession.perform(request: request) { [weak self] (result) in
            guard let self = self else {
                return
            }
            
            let httpResult: Result<T, Error>
            switch result {
            case .success(let response, let data):
                httpResult = self.handleFetchPhotosSuccessResponse(response: response, data: data)
            case .failure(let error):
                httpResult = .failure(error)
            }
            
            completion(httpResult)
        }
        
        return task
    }
    
    private func handleFetchPhotosSuccessResponse<T: Decodable>(response: URLResponse, data: Data) -> Result<T, Error> {
        let result: Result<T, HTTPResponseParserError> = responseParser.parse(response: response, data: data)
        
        switch result {

        case .success(let value):
            return .success(value)
        case .failure(let error):
            switch error {
            case .apiError(let response, let data):
                switch response.statusCode {
                case 401:
                    break
                case 422:
                    break
                default:
                    break
                }
            case .invalidResponse:
                return .failure(NSError())
            }
        }
        
        return .failure(NSError())
    }
    
    private func photosRequest(with searchTag: String, page: Int) -> URLRequest {
        let baseUrl = URL(string: "https://www.flickr.com/services/rest/")!
        guard var urlComponents = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true) else {
            assertionFailure("Couldn't create photos request!")
            return URLRequest(url: baseUrl)
        }

        let methodQuery = URLQueryItem(name: "method", value: "flickr.photos.search")
        let apiKeyQuery = URLQueryItem(name: "api_key", value: "59f1221c42b07eddaf48f3c7b3c1447d")
        let tagQuery = URLQueryItem(name: "tags", value: searchTag)
        let pageQuery = URLQueryItem(name: "page", value: String(page))
        let formatQuery = URLQueryItem(name: "format", value: "json")
        let noJsonQuery = URLQueryItem(name: "nojsoncallback", value: "1")
        
        urlComponents.queryItems = [methodQuery, apiKeyQuery, tagQuery, pageQuery, formatQuery, noJsonQuery]
        
        guard let url = urlComponents.url else {
            assertionFailure("Couldn't create photos request!")
            return URLRequest(url: baseUrl)
        }
        
        return URLRequest(url: url)
    }
}
