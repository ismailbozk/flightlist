//
//  URLSessionExtension.swift
//  Flight
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

public typealias ApiResult = Result<(response: URLResponse, data: Data), URLError>

/// Network Url Session protocol
public protocol NetworkUrlSession {
    /// Triggers a Network request, returns the netwrk data task which can be used to cancel an ongoing request.
    /// - Parameters:
    ///   - request: URLRequest object which will be sent to nextwork
    ///   - completion: completion closure from the nextwrok with data response pair or an network error.
    @discardableResult
    func perform(request: URLRequest, completion: @escaping (ApiResult) -> Void) -> NetworkUrlSessionDataTask
}

/// Network url session data task
public protocol NetworkUrlSessionDataTask {
    /// Cancels an ongoing network url request
    func cancel()
}

extension URLSessionDataTask: NetworkUrlSessionDataTask {}

extension URLSession: NetworkUrlSession {
    /// Network request for given request, returns the netwrk data task which can be used to cancel an ongoing request.
    /// - Parameters:
    ///   - request: URLRequest object which will be sent to nextwork
    ///   - completion: completion closure from the network with data response pair or an network error.
    @discardableResult
    public func perform(request: URLRequest, completion: @escaping (ApiResult) -> Void) -> NetworkUrlSessionDataTask {
        let task = dataTask(with: request) { (data, response, error) in
            let result: ApiResult
            if let response = response, let data = data {
                result = ApiResult.success((response: response, data: data))
            }
            else {
                let error = error as? URLError ?? URLError(URLError.badServerResponse)
                result = ApiResult.failure(error)
            }

            completion(result)
        }
        task.resume()
        return task
    }
}
