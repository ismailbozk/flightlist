//
//  DateFormatter.swift
//  Flight
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

public extension DateFormatter {
    
    /// "yyyy-MM-dd'T'HH:mm:ssZ"
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        formatter.timeZone = .autoupdatingCurrent
        return formatter
    }()
    
    static let displayDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = .autoupdatingCurrent
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        return formatter
    }()
    
}
