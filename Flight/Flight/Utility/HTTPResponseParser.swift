//
//  HTTPResponseParser.swift
//  Flight
//
//  Created by Bozkurt on 11/20/19.
//  Copyright © 2019 Bozkurt. All rights reserved.
//

import Foundation

public enum HTTPResponseParserError: Error {
    case apiError(response: HTTPURLResponse, data: Data)
    case invalidResponse
}

/// An Api client specialized for HTTP Requests.
final public class HTTPResponseParser {
    private let decoder: JSONDecoder
    
    init(decoder: JSONDecoder = JSONDecoder()) {
        self.decoder = decoder
    }
    
    public func parse<T: Decodable>(response: URLResponse, data: Data) -> Result<T, HTTPResponseParserError> {
        guard let httpResponse = response as? HTTPURLResponse else {
            return .failure(.invalidResponse)
        }
        
        switch httpResponse.statusCode {
        case 200...299:
            do {
                return .success(try decoder.decode(T.self, from: data))
            } catch {
                return .failure(.invalidResponse)
            }
        default:
            return .failure(.apiError(response: httpResponse, data: data))
        }
    }
}
